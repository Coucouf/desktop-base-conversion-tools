#!/bin/bash
set -e

SVN_REPO=${1:-desktop-base}
SVN_REPO_DIR=/var/svn/${SVN_REPO}
SVN_REPO_URL=http://localhost/svn/${SVN_REPO}/
echo "Repository name is '${SVN_REPO}'"
echo "  local dir: ${SVN_REPO_DIR}"
echo "  local url: ${SVN_REPO_URL}"

if [ ! -d ${SVN_REPO_DIR} ] ; then
    echo "Creating local repository structure"
    sudo svnadmin create ${SVN_REPO_DIR}
    echo "Setting local repository rights"
    sudo chown -R www-data:www-data ${SVN_REPO_DIR}/db/
    sudo chown -R www-data:www-data ${SVN_REPO_DIR}/locks/
    echo "Setting up repository hooks for synchronization"
    sudo cp ${SVN_REPO_DIR}/hooks/pre-revprop-change.tmpl ${SVN_REPO_DIR}/hooks/pre-revprop-change
    sudo sed -i -e "/if \[ \"\$ACTION\" = \"M\" -a \"\$PROPNAME\" = \"svn:log\" \]; then exit 0; fi/,+3s/^/#/" ${SVN_REPO_DIR}/hooks/pre-revprop-change

    echo "Initializing synchronization with Alioth repo"
    svnsync initialize ${SVN_REPO_URL} svn://svn.debian.org/debian-desktop/
else
    echo "'${SVN_REPO_DIR}' already exists, trying to continue…"
fi
echo "Synchronizing"
svnsync synchronize ${SVN_REPO_URL}
echo "All done."
